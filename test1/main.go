package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"
	"text/tabwriter"
)

func duplicate(arr []int64) {
	resultArr := make(map[int64]int)
	var result []int64
	for _, value := range arr {
		resultArr[value] = resultArr[value] + 1
	}

	for index, value := range resultArr {
		if value >= 2 {
			result = append(result, index)
		}
	}
	fmt.Println(result)
	fmt.Println()
}

// sort and input only 0, 1 and 2
func sortArr(arr []int64) {
	var arrInt []int
	for _, value := range arr {
		if value != 0 && value != 1 && value != 2 {
			fmt.Println("Angka yang diinput harus 0, 1 atau 2")
			return
		}
		arrInt = append(arrInt, int(value))
	}
	sort.Ints(arrInt)
	fmt.Println(arrInt)
	fmt.Println()
}

func maxProduct(arr []int64) {
	var current int64 = 1
	var min int64 = 1
	var max int64 = arr[0]
	var resultArr []int64

	for i := 0; i < len(arr); i++ {
		switch {
		case arr[i] > 0:
			current, min = arr[i]*current, arr[i]*min

		case arr[i] < 0:
			current, min = arr[i]*min, arr[i]*current

		default:
			current = 0
			min = 1
		}

		if max < current {
			max = current
			resultArr = append(resultArr, arr[i])
		}

		if current <= 0 {
			current = 1
		}
	}

	fmt.Println("the maximum product sub-array is", resultArr, "having product", max)
	fmt.Println()
}

func permutation(arr []int64) {
	var resultArr [][]int64

	for i := 0; i < len(arr); i++ {
		for j := i; j < len(arr); j++ {
			var result []int64
			result = append(result, arr[i])
			result = append(result, arr[j])
			resultArr = append(resultArr, result)
		}
	}
	fmt.Println(resultArr)
	fmt.Println()
}

func help() {
	w := tabwriter.NewWriter(os.Stdout, 1, 1, 4, ' ', tabwriter.TabIndent|tabwriter.StripEscape)
	fmt.Fprintln(w, " Usage:")
	fmt.Fprintln(w, "    <command> <array of integer>")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, " The commands are:")
	fmt.Fprintln(w, "    soal-1\tFind the duplicate number in array. \tExample soal-1 [1,2,3,4,4]")
	fmt.Fprintln(w, "    soal-2\tSort the array in linear time and using constant space. input array containing only 0's, 1's and 2's. \tExample soal-2 [0,1,2,2,1,0,0,2,0,1,1,0]")
	fmt.Fprintln(w, "    soal-3\tFind all distinct combinations of given length where repetition of elements is allowed. \tExample soal-3 [1,2,3]")
	fmt.Fprintln(w, "    soal-4\tFind maximum product (multiplication) subarray. \tExample soal-4 [-6,4,-5,8,-10,0,8]")
	fmt.Fprintln(w, "    exit \tClose Application")
	fmt.Fprintln(w, "")
	fmt.Fprintln(w, "")
}

func runCommand(command string) {
	command = strings.TrimSuffix(command, "\n")
	arrCommandStr := strings.Fields(command)
	var arr []int64

	if len(arrCommandStr) == 2 {
		err := json.Unmarshal([]byte(arrCommandStr[1]), &arr)
		if err != nil {
			fmt.Println(`try "help" for get list of command`)
			return
		}
	} else if arrCommandStr[0] != "help" && arrCommandStr[0] != "exit" {
		fmt.Println(`try "help" for get list of command`)
		return
	}

	switch arrCommandStr[0] {
	case "soal-1":
		duplicate(arr)
		return
	case "soal-2":
		sortArr(arr)
		return
	case "soal-3":
		permutation(arr)
		return
	case "soal-4":
		maxProduct(arr)
		return
	case "help":
		help()
		return
	case "exit":
		os.Exit(0)
		return
	default:
		output := `try "help" for get list of command`
		fmt.Println(output)
		return
	}
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("$ ")
		cmdString, err := reader.ReadString('\n')
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		runCommand(cmdString)
	}
}
