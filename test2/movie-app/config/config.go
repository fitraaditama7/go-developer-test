package config

import (
	"fmt"

	"github.com/spf13/viper"
)

var (
	POSTGRESDRIVER = ""
	POSTGRESURL    = ""
)

func Load() {
	viper.SetConfigFile("./config.json")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	postgresUser := viper.GetString(`postgres.db_user`)
	postgresPort := viper.GetString(`postgres.port`)
	postgresHost := viper.GetString(`postgres.host`)
	postgresDbName := viper.GetString(`postgres.db_name`)
	postgresDbPassword := viper.GetString(`postgres.password`)
	POSTGRESDRIVER = viper.GetString(`postgres.driver`)
	POSTGRESURL = fmt.Sprintf("postgres://%s:%s@%s:%s/%s", postgresUser, postgresDbPassword, postgresHost, postgresPort, postgresDbName)
	POSTGRESURL = fmt.Sprintf("%s?sslmode=disable", POSTGRESURL)
}
