package helper

const ErrInternalServerError = "Internal Server Error"
const ErrNotFound = "Not Found"
const ErrForbidden = "Forbidden"
const ErrBadRequest = "Bad Request"
const ErrBadGateway = "Bad Gateway"
