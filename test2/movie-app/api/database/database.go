package database

import (
	"database/sql"
	"movie-app/config"

	_ "github.com/lib/pq"
)

func Load() (*sql.DB, error) {
	db, err := sql.Open("postgres", config.POSTGRESURL)
	if err != nil {
		return nil, err
	}
	return db, nil
}
