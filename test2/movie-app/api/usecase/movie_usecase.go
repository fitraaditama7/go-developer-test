package repository

import (
	"context"
	"movie-app/api/models"
)

type MovieUsecase interface {
	List(ctx context.Context, filter interface{}) ([]*models.MovieList, error)
	Insert(ctx context.Context, model *models.Movie) (*models.Movie, error)
	Update(ctx context.Context, data map[string]interface{}, id int) (*models.Movie, error)
	Detail(ctx context.Context, id int) (*models.Movie, error)
	Delete(ctx context.Context, id int) error
}
