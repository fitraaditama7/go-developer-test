package repository

import (
	"context"
	"movie-app/api/models"
)

type SourceUsecase interface {
	List(ctx context.Context) ([]*models.Source, error)
	Insert(ctx context.Context, model *models.Source) (*models.Source, error)
	Update(ctx context.Context, data map[string]interface{}, id int) (*models.Source, error)
	Detail(ctx context.Context, id int) (*models.Source, error)
	Delete(ctx context.Context, id int) error
}
