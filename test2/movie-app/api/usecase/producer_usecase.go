package repository

import (
	"context"
	"movie-app/api/models"
)

type ProducerUsecase interface {
	List(ctx context.Context) ([]*models.Producer, error)
	Insert(ctx context.Context, model *models.Producer) (*models.Producer, error)
	Update(ctx context.Context, data map[string]interface{}, id int) (*models.Producer, error)
	Detail(ctx context.Context, id int) (*models.Producer, error)
	Delete(ctx context.Context, id int) error
}
