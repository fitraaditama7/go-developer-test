package repository

import (
	"context"
	"movie-app/api/models"
)

type StudioUsecase interface {
	List(ctx context.Context) ([]*models.Studio, error)
	Insert(ctx context.Context, model *models.Studio) (*models.Studio, error)
	Update(ctx context.Context, data map[string]interface{}, id int) (*models.Studio, error)
	Detail(ctx context.Context, id int) (*models.Studio, error)
	Delete(ctx context.Context, id int) error
}
