package repository

import (
	"context"
	"movie-app/api/models"
)

type GenreUsecase interface {
	List(ctx context.Context) ([]*models.Genre, error)
	Insert(ctx context.Context, model *models.Genre) (*models.Genre, error)
	Update(ctx context.Context, data map[string]interface{}, id int) (*models.Genre, error)
	Detail(ctx context.Context, id int) (*models.Genre, error)
	Delete(ctx context.Context, id int) error
}
