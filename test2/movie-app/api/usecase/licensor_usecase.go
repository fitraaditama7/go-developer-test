package repository

import (
	"context"
	"movie-app/api/models"
)

type LicensorUsecase interface {
	List(ctx context.Context) ([]*models.Licensor, error)
	Insert(ctx context.Context, model *models.Licensor) (*models.Licensor, error)
	Update(ctx context.Context, data map[string]interface{}, id int) (*models.Licensor, error)
	Detail(ctx context.Context, id int) (*models.Licensor, error)
	Delete(ctx context.Context, id int) error
}
