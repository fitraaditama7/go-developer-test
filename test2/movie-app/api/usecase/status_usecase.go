package repository

import (
	"context"
	"movie-app/api/models"
)

type StatusUsecase interface {
	List(ctx context.Context) ([]*models.Status, error)
	Insert(ctx context.Context, model *models.Status) (*models.Status, error)
	Update(ctx context.Context, data map[string]interface{}, id int) (*models.Status, error)
	Detail(ctx context.Context, id int) (*models.Status, error)
	Delete(ctx context.Context, id int) error
}
