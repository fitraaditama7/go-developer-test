package repository

import (
	"context"
	"movie-app/api/models"
)

type RatingUsecase interface {
	List(ctx context.Context) ([]*models.Rating, error)
	Insert(ctx context.Context, model *models.Rating) (*models.Rating, error)
	Update(ctx context.Context, data map[string]interface{}, id int) (*models.Rating, error)
	Detail(ctx context.Context, id int) (*models.Rating, error)
	Delete(ctx context.Context, id int) error
}
