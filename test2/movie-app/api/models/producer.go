package models

type Producer struct {
	ID    int
	Title string
}
