package models

type Movie struct {
	ID        int
	Title     string
	Cover     string
	Aired     string
	Duration  string
	Synopsis  string
	Genres    []string
	Score     float64
	Producers []Producer
	Status    Status
	Studio    Studio
	Licensor  Licensor
	Source    Source
	Rating    Rating
}
