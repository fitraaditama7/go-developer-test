package models

type Genre struct {
	ID    int
	Title string
}
