package models

type Licensor struct {
	ID    int
	Title string
}
