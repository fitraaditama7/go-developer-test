package models

type MovieList struct {
	ID       int
	Title    string
	Cover    string
	Aired    string
	Synopsis string
	Genres   []string
	Status   string
	Score    float64
}
