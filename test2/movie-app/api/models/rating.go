package models

type Rating struct {
	ID    int
	Title string
}
