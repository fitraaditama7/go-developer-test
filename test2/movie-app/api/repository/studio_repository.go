package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"movie-app/api/helper"
	"movie-app/api/models"
	"strings"

	"github.com/sirupsen/logrus"
)

type StudioRepository interface {
	List(ctx context.Context) ([]*models.Studio, error)
	Insert(ctx context.Context, title string) (*models.Studio, error)
	Update(ctx context.Context, data map[string]interface{}, id int) error
	Detail(ctx context.Context, id int) (*models.Studio, error)
	Delete(ctx context.Context, id int) error
}

type psqlStudioRepository struct {
	Conn *sql.DB
}

func NewStudioRepository(Conn *sql.DB) StudioRepository {
	return &psqlStudioRepository{Conn}
}

func (p *psqlStudioRepository) List(ctx context.Context) ([]*models.Studio, error) {
	var studioList []*models.Studio

	query := `SELECT id, title from tm_studio`
	rows, err := p.Conn.QueryContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in StudioRepository List. Can't prepare query got error: %v", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		row := new(models.Studio)
		err := rows.Scan(
			&row.ID,
			&row.Title,
		)
		if err != nil && err != sql.ErrNoRows {
			logrus.Errorf("Error in StudioRepository List. Can't prepare query got error: %v", err.Error())
			return nil, errors.New(helper.ErrInternalServerError)
		}
		studioList = append(studioList, row)
	}
	return studioList, nil
}

func (p *psqlStudioRepository) Detail(ctx context.Context, id int) (*models.Studio, error) {
	var studio *models.Studio
	query := `SELECT id, title FROM tm_studio WHERE id = $1`
	err := p.Conn.QueryRowContext(ctx, query, id).Scan(
		&studio.ID,
		&studio.Title,
	)

	if err != nil {
		logrus.Errorf("Error in StudioRepository Detail. Can't exec query got error: %v", err.Error())
		return nil, err
	}

	return studio, nil
}

func (p *psqlStudioRepository) Insert(ctx context.Context, title string) (*models.Studio, error) {
	var studio models.Studio
	studio.Title = title

	query := `INSERT INTO tm_studio (title, created_at) VALUES ($1, NOW()) RETURNING id`
	err := p.Conn.QueryRowContext(ctx, query, title).Scan(&studio.ID)
	if err != nil && err != sql.ErrNoRows {
		logrus.Errorf("Error in StudioRepository Insert. Can't prepare query got error: %v", err.Error())
		return nil, errors.New(helper.ErrInternalServerError)
	}
	return &studio, nil
}

func (p *psqlStudioRepository) Update(ctx context.Context, data map[string]interface{}, id int) error {
	query := `UPDATE tm_studio SET updated_at = NOW()`

	var val []interface{}
	var i = 1

	for index, value := range data {
		if value != nil {
			query = fmt.Sprintf("%s %s = $%d,", query, index, i)
			val = append(val, value)
			i++
		}
	}
	val = append(val, id)

	query = strings.TrimRight(query, ", ")
	query = fmt.Sprintf(`%s WHERE id = $%d`, query, i+1)

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in StudioRepository Update. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in StudioRepository Update. Can't close statemant got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = p.Conn.ExecContext(ctx, query, val...)
	if err != nil {
		logrus.Errorf("Error in StudioRepository Update. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}

func (p *psqlStudioRepository) Delete(ctx context.Context, id int) error {
	query := `UPDATE tm_studio SET deleted_at = NOW()`

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in StudioRepository Delete. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}

	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in StudioRepository Delete. Can't close statement got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		logrus.Errorf("Error in StudioRepository Delete. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}
