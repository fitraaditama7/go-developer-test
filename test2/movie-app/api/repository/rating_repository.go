package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"movie-app/api/helper"
	"movie-app/api/models"
	"strings"

	"github.com/sirupsen/logrus"
)

type RatingRepository interface {
	List(ctx context.Context) ([]*models.Rating, error)
	Insert(ctx context.Context, title string) (*models.Rating, error)
	Update(ctx context.Context, data map[string]interface{}, id int) error
	Detail(ctx context.Context, id int) (*models.Rating, error)
	Delete(ctx context.Context, id int) error
}

type psqlRatingRepository struct {
	Conn *sql.DB
}

func NewRatingRepository(Conn *sql.DB) RatingRepository {
	return &psqlRatingRepository{Conn}
}

func (p *psqlRatingRepository) List(ctx context.Context) ([]*models.Rating, error) {
	var ratings []*models.Rating

	query := `SELECT id, title from tm_rating`
	rows, err := p.Conn.QueryContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in RatingRepository List. Can't prepare query got error: %v", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		row := new(models.Rating)
		err := rows.Scan(
			&row.ID,
			&row.Title,
		)
		if err != nil && err != sql.ErrNoRows {
			logrus.Errorf("Error in RatingRepository List. Can't prepare query got error: %v", err.Error())
			return nil, errors.New(helper.ErrInternalServerError)
		}
		ratings = append(ratings, row)
	}
	return ratings, nil
}

func (p *psqlRatingRepository) Detail(ctx context.Context, id int) (*models.Rating, error) {
	var rating *models.Rating
	query := `SELECT id, title FROM tm_rating WHERE id = $1`
	err := p.Conn.QueryRowContext(ctx, query, id).Scan(
		&rating.ID,
		&rating.Title,
	)

	if err != nil {
		logrus.Errorf("Error in RatingRepository Detail. Can't exec query got error: %v", err.Error())
		return nil, err
	}

	return rating, nil
}

func (p *psqlRatingRepository) Insert(ctx context.Context, title string) (*models.Rating, error) {
	var rating models.Rating
	rating.Title = title

	query := `INSERT INTO tm_rating (title, created_at) VALUES ($1, NOW()) RETURNING id`
	err := p.Conn.QueryRowContext(ctx, query, title).Scan(&rating.ID)
	if err != nil && err != sql.ErrNoRows {
		logrus.Errorf("Error in RatingRepository Insert. Can't prepare query got error: %v", err.Error())
		return nil, errors.New(helper.ErrInternalServerError)
	}
	return &rating, nil
}

func (p *psqlRatingRepository) Update(ctx context.Context, data map[string]interface{}, id int) error {
	query := `UPDATE tm_rating SET updated_at = NOW()`

	var val []interface{}
	var i = 1

	for index, value := range data {
		if value != nil {
			query = fmt.Sprintf("%s %s = $%d,", query, index, i)
			val = append(val, value)
			i++
		}
	}
	val = append(val, id)

	query = strings.TrimRight(query, ", ")
	query = fmt.Sprintf(`%s WHERE id = $%d`, query, i+1)

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in RatingRepository Update. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in RatingRepository Update. Can't close statemant got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = p.Conn.ExecContext(ctx, query, val...)
	if err != nil {
		logrus.Errorf("Error in RatingRepository Update. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}

func (p *psqlRatingRepository) Delete(ctx context.Context, id int) error {
	query := `UPDATE tm_rating SET deleted_at = NOW()`

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in RatingRepository Delete. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}

	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in RatingRepository Delete. Can't close statement got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		logrus.Errorf("Error in RatingRepository Delete. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}
