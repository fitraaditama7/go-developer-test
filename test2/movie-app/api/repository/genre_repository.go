package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"movie-app/api/helper"
	"movie-app/api/models"
	"strings"

	"github.com/sirupsen/logrus"
)

type GenreRepsitory interface {
	List(ctx context.Context) ([]*models.Genre, error)
	Insert(ctx context.Context, title string) (*models.Genre, error)
	Update(ctx context.Context, data map[string]interface{}, id int) error
	Detail(ctx context.Context, id int) (*models.Genre, error)
	Delete(ctx context.Context, id int) error
}

type psqlGenreRepository struct {
	Conn *sql.DB
}

func NewGenreRepository(Conn *sql.DB) GenreRepsitory {
	return &psqlGenreRepository{Conn}
}

func (p *psqlGenreRepository) List(ctx context.Context) ([]*models.Genre, error) {
	var genres []*models.Genre

	query := `SELECT id, title from tm_genre`
	rows, err := p.Conn.QueryContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in GenreRepository List. Can't prepare query got error: %v", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		row := new(models.Genre)
		err := rows.Scan(
			&row.ID,
			&row.Title,
		)
		if err != nil && err != sql.ErrNoRows {
			logrus.Errorf("Error in GenreRepository List. Can't prepare query got error: %v", err.Error())
			return nil, errors.New(helper.ErrInternalServerError)
		}
		genres = append(genres, row)
	}
	return genres, nil
}

func (p *psqlGenreRepository) Detail(ctx context.Context, id int) (*models.Genre, error) {
	var genre *models.Genre
	query := `SELECT id, title FROM tm_genre WHERE id = $1`
	err := p.Conn.QueryRowContext(ctx, query, id).Scan(
		&genre.ID,
		&genre.Title,
	)

	if err != nil {
		logrus.Errorf("Error in GenreRepository Detail. Can't exec query got error: %v", err.Error())
		return nil, err
	}

	return genre, nil
}

func (p *psqlGenreRepository) Insert(ctx context.Context, title string) (*models.Genre, error) {
	var genre models.Genre
	genre.Title = title

	query := `INSERT INTO tm_genre (title, created_at) VALUES ($1, NOW()) RETURNING id`
	err := p.Conn.QueryRowContext(ctx, query, title).Scan(&genre.ID)
	if err != nil && err != sql.ErrNoRows {
		logrus.Errorf("Error in GenreRepository Insert. Can't prepare query got error: %v", err.Error())
		return nil, errors.New(helper.ErrInternalServerError)
	}
	return &genre, nil
}

func (p *psqlGenreRepository) Update(ctx context.Context, data map[string]interface{}, id int) error {
	query := `UPDATE tm_genre SET updated_at = NOW()`

	var val []interface{}
	var i = 1

	for index, value := range data {
		if value != nil {
			query = fmt.Sprintf("%s %s = $%d,", query, index, i)
			val = append(val, value)
			i++
		}
	}
	val = append(val, id)

	query = strings.TrimRight(query, ", ")
	query = fmt.Sprintf(`%s WHERE id = $%d`, query, i+1)

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in GenreRepository Update. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in GenreRepository Update. Can't close statemant got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = p.Conn.ExecContext(ctx, query, val...)
	if err != nil {
		logrus.Errorf("Error in GenreRepository Update. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}

func (p *psqlGenreRepository) Delete(ctx context.Context, id int) error {
	query := `UPDATE tm_genre SET deleted_at = NOW()`

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in GenreRepository Delete. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}

	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in GenreRepository Delete. Can't close statement got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		logrus.Errorf("Error in GenreRepository Delete. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}
