package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"movie-app/api/helper"
	"movie-app/api/models"
	"strings"

	"github.com/sirupsen/logrus"
)

type StatusRepository interface {
	List(ctx context.Context) ([]*models.Status, error)
	Insert(ctx context.Context, title string) (*models.Status, error)
	Update(ctx context.Context, data map[string]interface{}, id int) error
	Detail(ctx context.Context, id int) (*models.Status, error)
	Delete(ctx context.Context, id int) error
}

type psqlStatusRepository struct {
	Conn *sql.DB
}

func NewStatusRepository(Conn *sql.DB) StatusRepository {
	return &psqlStatusRepository{Conn}
}

func (p *psqlStatusRepository) List(ctx context.Context) ([]*models.Status, error) {
	var statusList []*models.Status

	query := `SELECT id, title from tm_status`
	rows, err := p.Conn.QueryContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in StatusRepository List. Can't prepare query got error: %v", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		row := new(models.Status)
		err := rows.Scan(
			&row.ID,
			&row.Title,
		)
		if err != nil && err != sql.ErrNoRows {
			logrus.Errorf("Error in StatusRepository List. Can't prepare query got error: %v", err.Error())
			return nil, errors.New(helper.ErrInternalServerError)
		}
		statusList = append(statusList, row)
	}
	return statusList, nil
}

func (p *psqlStatusRepository) Detail(ctx context.Context, id int) (*models.Status, error) {
	var status *models.Status
	query := `SELECT id, title FROM tm_status WHERE id = $1`
	err := p.Conn.QueryRowContext(ctx, query, id).Scan(
		&status.ID,
		&status.Title,
	)

	if err != nil {
		logrus.Errorf("Error in StatusRepository Detail. Can't exec query got error: %v", err.Error())
		return nil, err
	}

	return status, nil
}

func (p *psqlStatusRepository) Insert(ctx context.Context, title string) (*models.Status, error) {
	var status models.Status
	status.Title = title

	query := `INSERT INTO tm_status (title, created_at) VALUES ($1, NOW()) RETURNING id`
	err := p.Conn.QueryRowContext(ctx, query, title).Scan(&status.ID)
	if err != nil && err != sql.ErrNoRows {
		logrus.Errorf("Error in StatusRepository Insert. Can't prepare query got error: %v", err.Error())
		return nil, errors.New(helper.ErrInternalServerError)
	}
	return &status, nil
}

func (p *psqlStatusRepository) Update(ctx context.Context, data map[string]interface{}, id int) error {
	query := `UPDATE tm_status SET updated_at = NOW()`

	var val []interface{}
	var i = 1

	for index, value := range data {
		if value != nil {
			query = fmt.Sprintf("%s %s = $%d,", query, index, i)
			val = append(val, value)
			i++
		}
	}
	val = append(val, id)

	query = strings.TrimRight(query, ", ")
	query = fmt.Sprintf(`%s WHERE id = $%d`, query, i+1)

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in StatusRepository Update. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in StatusRepository Update. Can't close statemant got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = p.Conn.ExecContext(ctx, query, val...)
	if err != nil {
		logrus.Errorf("Error in StatusRepository Update. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}

func (p *psqlStatusRepository) Delete(ctx context.Context, id int) error {
	query := `UPDATE tm_status SET deleted_at = NOW()`

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in StatusRepository Delete. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}

	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in StatusRepository Delete. Can't close statement got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		logrus.Errorf("Error in StatusRepository Delete. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}
