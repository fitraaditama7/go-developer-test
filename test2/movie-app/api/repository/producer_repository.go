package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"movie-app/api/helper"
	"movie-app/api/models"
	"strings"

	"github.com/sirupsen/logrus"
)

type ProducerRepository interface {
	List(ctx context.Context) ([]*models.Producer, error)
	// ListById(ctx context.Context, id []int) ([]*models.Producer, error)
	Insert(ctx context.Context, title string) (*models.Producer, error)
	Update(ctx context.Context, data map[string]interface{}, id int) error
	Detail(ctx context.Context, id int) (*models.Producer, error)
	Delete(ctx context.Context, id int) error
}
type psqlProducerRepository struct {
	Conn *sql.DB
}

func NewProducerRepository(Conn *sql.DB) ProducerRepository {
	return &psqlProducerRepository{Conn}
}

func (p *psqlProducerRepository) List(ctx context.Context) ([]*models.Producer, error) {
	var producers []*models.Producer

	query := `SELECT id, title from tm_producer`
	rows, err := p.Conn.QueryContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in ProducerRepository List. Can't prepare query got error: %v", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		row := new(models.Producer)
		err := rows.Scan(
			&row.ID,
			&row.Title,
		)
		if err != nil && err != sql.ErrNoRows {
			logrus.Errorf("Error in ProducerRepository List. Can't prepare query got error: %v", err.Error())
			return nil, errors.New(helper.ErrInternalServerError)
		}
		producers = append(producers, row)
	}
	return producers, nil
}

func (p *psqlProducerRepository) Detail(ctx context.Context, id int) (*models.Producer, error) {
	var producer *models.Producer
	query := `SELECT id, title FROM tm_producer WHERE id = $1`
	err := p.Conn.QueryRowContext(ctx, query, id).Scan(
		&producer.ID,
		&producer.Title,
	)

	if err != nil {
		logrus.Errorf("Error in ProducerRepository Detail. Can't exec query got error: %v", err.Error())
		return nil, err
	}

	return producer, nil
}

func (p *psqlProducerRepository) Insert(ctx context.Context, title string) (*models.Producer, error) {
	var producer models.Producer
	producer.Title = title

	query := `INSERT INTO tm_producer (title, created_at) VALUES ($1, NOW()) RETURNING id`
	err := p.Conn.QueryRowContext(ctx, query, title).Scan(&producer.ID)
	if err != nil && err != sql.ErrNoRows {
		logrus.Errorf("Error in ProducerRepository Insert. Can't prepare query got error: %v", err.Error())
		return nil, errors.New(helper.ErrInternalServerError)
	}
	return &producer, nil
}

func (p *psqlProducerRepository) Update(ctx context.Context, data map[string]interface{}, id int) error {
	query := `UPDATE tm_producer SET updated_at = NOW()`

	var val []interface{}
	var i = 1

	for index, value := range data {
		if value != nil {
			query = fmt.Sprintf("%s %s = $%d,", query, index, i)
			val = append(val, value)
			i++
		}
	}
	val = append(val, id)

	query = strings.TrimRight(query, ", ")
	query = fmt.Sprintf(`%s WHERE id = $%d`, query, i+1)

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in ProducerRepository Update. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in ProducerRepository Update. Can't close statemant got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = p.Conn.ExecContext(ctx, query, val...)
	if err != nil {
		logrus.Errorf("Error in ProducerRepository Update. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}

func (p *psqlProducerRepository) Delete(ctx context.Context, id int) error {
	query := `UPDATE tm_producer SET deleted_at = NOW()`

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in ProducerRepository Delete. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}

	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in ProducerRepository Delete. Can't close statement got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		logrus.Errorf("Error in ProducerRepository Delete. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}
