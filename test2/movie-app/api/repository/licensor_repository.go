package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"movie-app/api/helper"
	"movie-app/api/models"
	"strings"

	"github.com/sirupsen/logrus"
)

type LicensorRepository interface {
	List(ctx context.Context) ([]*models.Licensor, error)
	// ListById(ctx context.Context, id []int) ([]*models.Licensor, error)
	Insert(ctx context.Context, title string) (*models.Licensor, error)
	Update(ctx context.Context, data map[string]interface{}, id int) error
	Detail(ctx context.Context, id int) (*models.Licensor, error)
	Delete(ctx context.Context, id int) error
}

type psqlLicensorRepository struct {
	Conn *sql.DB
}

func NewLicensorRepository(Conn *sql.DB) LicensorRepository {
	return &psqlLicensorRepository{Conn}
}

func (p *psqlLicensorRepository) List(ctx context.Context) ([]*models.Licensor, error) {
	var licensors []*models.Licensor

	query := `SELECT id, title from tm_licensor`
	rows, err := p.Conn.QueryContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in LicensorRepository List. Can't prepare query got error: %v", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		row := new(models.Licensor)
		err := rows.Scan(
			&row.ID,
			&row.Title,
		)
		if err != nil && err != sql.ErrNoRows {
			logrus.Errorf("Error in LicensorRepository List. Can't prepare query got error: %v", err.Error())
			return nil, errors.New(helper.ErrInternalServerError)
		}
		licensors = append(licensors, row)
	}
	return licensors, nil
}

func (p *psqlLicensorRepository) Detail(ctx context.Context, id int) (*models.Licensor, error) {
	var licensor *models.Licensor
	query := `SELECT id, title FROM tm_licensor WHERE id = $1`
	err := p.Conn.QueryRowContext(ctx, query, id).Scan(
		&licensor.ID,
		&licensor.Title,
	)

	if err != nil {
		logrus.Errorf("Error in LicensorRepository Detail. Can't exec query got error: %v", err.Error())
		return nil, err
	}

	return licensor, nil
}

func (p *psqlLicensorRepository) Insert(ctx context.Context, title string) (*models.Licensor, error) {
	var licensor models.Licensor
	licensor.Title = title

	query := `INSERT INTO tm_licensor (title, created_at) VALUES ($1, NOW()) RETURNING id`
	err := p.Conn.QueryRowContext(ctx, query, title).Scan(&licensor.ID)
	if err != nil && err != sql.ErrNoRows {
		logrus.Errorf("Error in LicensorRepository Insert. Can't prepare query got error: %v", err.Error())
		return nil, errors.New(helper.ErrInternalServerError)
	}
	return &licensor, nil
}

func (p *psqlLicensorRepository) Update(ctx context.Context, data map[string]interface{}, id int) error {
	query := `UPDATE tm_licensor SET updated_at = NOW()`

	var val []interface{}
	var i = 1

	for index, value := range data {
		if value != nil {
			query = fmt.Sprintf("%s %s = $%d,", query, index, i)
			val = append(val, value)
			i++
		}
	}
	val = append(val, id)

	query = strings.TrimRight(query, ", ")
	query = fmt.Sprintf(`%s WHERE id = $%d`, query, i+1)

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in LicensorRepository Update. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in LicensorRepository Update. Can't close statemant got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = p.Conn.ExecContext(ctx, query, val...)
	if err != nil {
		logrus.Errorf("Error in LicensorRepository Update. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}

func (p *psqlLicensorRepository) Delete(ctx context.Context, id int) error {
	query := `UPDATE tm_licensor SET deleted_at = NOW()`

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in LicensorRepository Delete. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}

	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in LicensorRepository Delete. Can't close statement got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		logrus.Errorf("Error in LicensorRepository Delete. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}
