package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"movie-app/api/helper"
	"movie-app/api/models"
	"strings"

	"github.com/sirupsen/logrus"
)

type SourceRepository interface {
	List(ctx context.Context) ([]*models.Source, error)
	Insert(ctx context.Context, title string) (*models.Source, error)
	Update(ctx context.Context, data map[string]interface{}, id int) error
	Detail(ctx context.Context, id int) (*models.Source, error)
	Delete(ctx context.Context, id int) error
}

type psqlSourceRepository struct {
	Conn *sql.DB
}

func NewSourceRepository(Conn *sql.DB) SourceRepository {
	return &psqlSourceRepository{Conn}
}

func (p *psqlSourceRepository) List(ctx context.Context) ([]*models.Source, error) {
	var sources []*models.Source

	query := `SELECT id, title from tm_source`
	rows, err := p.Conn.QueryContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in SourceRepository List. Can't prepare query got error: %v", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		row := new(models.Source)
		err := rows.Scan(
			&row.ID,
			&row.Title,
		)
		if err != nil && err != sql.ErrNoRows {
			logrus.Errorf("Error in SourceRepository List. Can't prepare query got error: %v", err.Error())
			return nil, errors.New(helper.ErrInternalServerError)
		}
		sources = append(sources, row)
	}
	return sources, nil
}

func (p *psqlSourceRepository) Detail(ctx context.Context, id int) (*models.Source, error) {
	var source *models.Source
	query := `SELECT id, title FROM tm_source WHERE id = $1`
	err := p.Conn.QueryRowContext(ctx, query, id).Scan(
		&source.ID,
		&source.Title,
	)

	if err != nil {
		logrus.Errorf("Error in SourceRepository Detail. Can't exec query got error: %v", err.Error())
		return nil, err
	}

	return source, nil
}

func (p *psqlSourceRepository) Insert(ctx context.Context, title string) (*models.Source, error) {
	var source models.Source
	source.Title = title

	query := `INSERT INTO tm_source (title, created_at) VALUES ($1, NOW()) RETURNING id`
	err := p.Conn.QueryRowContext(ctx, query, title).Scan(&source.ID)
	if err != nil && err != sql.ErrNoRows {
		logrus.Errorf("Error in SourceRepository Insert. Can't prepare query got error: %v", err.Error())
		return nil, errors.New(helper.ErrInternalServerError)
	}
	return &source, nil
}

func (p *psqlSourceRepository) Update(ctx context.Context, data map[string]interface{}, id int) error {
	query := `UPDATE tm_source SET updated_at = NOW()`

	var val []interface{}
	var i = 1

	for index, value := range data {
		if value != nil {
			query = fmt.Sprintf("%s %s = $%d,", query, index, i)
			val = append(val, value)
			i++
		}
	}
	val = append(val, id)

	query = strings.TrimRight(query, ", ")
	query = fmt.Sprintf(`%s WHERE id = $%d`, query, i+1)

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in SourceRepository Update. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in SourceRepository Update. Can't close statemant got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = p.Conn.ExecContext(ctx, query, val...)
	if err != nil {
		logrus.Errorf("Error in SourceRepository Update. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}

func (p *psqlSourceRepository) Delete(ctx context.Context, id int) error {
	query := `UPDATE tm_source SET deleted_at = NOW()`

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		logrus.Errorf("Error in SourceRepository Delete. Can't prepare query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}

	defer func() {
		err := stmt.Close()
		if err != nil {
			logrus.Errorf("Error in SourceRepository Delete. Can't close statement got error: %v", err.Error())
			panic(err)
		}
	}()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		logrus.Errorf("Error in SourceRepository Delete. Can't exec query got error: %v", err.Error())
		return errors.New(helper.ErrInternalServerError)
	}
	return nil
}
