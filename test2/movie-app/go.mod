module movie-app

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/lib/pq v1.6.0 // indirect
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/spf13/viper v1.7.0 // indirect
	github.com/stretchr/testify v1.6.0 // indirect
)
